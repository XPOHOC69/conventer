$.jqplot('chartdiv',  [[[1,34000],[2,35000],[3,35000],[4,35000],[5,35000],[6,40000],[7,51000],[8,60000],[9,70000]]]);

options =
{
// Plugin and renderer options.

// BarRenderer.
// With BarRenderer, you can specify additional options in the rendererOptions object
// on the series or on the seriesDefaults object.  Note, some options are respecified
// (like shadowDepth) to override lineRenderer defaults from which BarRenderer inherits.

seriesDefaults: {
    rendererOptions: {
        barPadding: 8,      // number of pixels between adjacent bars in the same
            // group (same category or bin).
            barMargin: 10,      // number of pixels between adjacent groups of bars.
            barDirection: 'vertical', // vertical or horizontal.
            barWidth: null,     // width of the bars.  null to calculate automatically.
            shadowOffset: 2,    // offset from the bar edge to stroke the shadow.
            shadowDepth: 5,     // nuber of strokes to make for the shadow.
            shadowAlpha: 0.8,   // transparency of the shadow.
    }
}};