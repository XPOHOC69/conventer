<?php
include_once(Yii::getPathOfAlias('application.extensions.mpdf60') . '/mpdf.php');

class SiteController extends Controller
{

    public function actionIndex()
    {
        $modelReport = new Report();
        $modelGroup = new Group();
        $groups = Group::model()->findAll();

        if (isset($_POST['Group'])) {
            $modelGroup->attributes = $_POST['Group'];
            if ($modelGroup->save()) {
                $modelGroup->unsetAttributes();
            }
        }

        if (isset($_POST['Report'])) {
            $modelReport->attributes = $_POST['Report'];
            if ($modelReport->save()) {
                $arr1 = explode("\n", $modelReport->keyword_price);//разбиваем текст на массив со значениями слово-цена
                foreach ($arr1 as $row) {
                    list($keyword, $price) = explode("-",
                        $row);//разбиваем значения из массива в переменны $keyword и $price
                    $keyword = trim($keyword);
                    $price = trim($price);
                    $arr2[$keyword] = $price;//записываем в новый массив ключ $keyword и значение $price
                }

                $page = $this->renderPartial('page', array('model' => $modelReport, 'keywords' => $arr2), true);

                $mpdf = new mPDF('utf-8', 'A4', '', '', 0, 0, 5, 5, 9, 0);
                $mpdf->SetFooter(" |{PAGENO}|");
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/style.css');
                $mpdf->WriteHTML($stylesheet, 1);
                $mpdf->setAutoTopMargin = 'pad';
                $mpdf->AddPage('', '', '', '', 'on');
                $mpdf->SetHTMLHeader('<div style="margin-right: 55px;">
                                            <div class="g-float-right b-logo-block">
                                                <img src="/images/css/logo2.png" class="g-float-right b-logo">
                                            </div>
                                       </div>', '');//устанавливаем логотип для каждой страницы начиная со второй*/
                $mpdf->WriteHTML($page, 2);
                $mpdf->Output('mpdf.pdf', 'I');
            }
        }

        if (isset($_GET['delete'])) {
            $id = (int)$_GET['delete'];
            Report::model()->findByPk($id)->delete();
        }

        if (isset($_GET['id'])) {
            $id = (int)$_GET['id'];
            $modelReport = Report::model()->findByPk($id);
        }

        $this->render('index', array(
            'modelReport' => $modelReport,
            'modelGroup'  => $modelGroup,
            'groups'      => $groups,
        ));
    }


    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) {
                echo $error['message'];
            } else {
                $this->render('error', $error);
            }
        }
    }
}