<div class="b-list-text">
    <div class="g-margin-5-top  b-margin-text-bottom3 b-h2">Отчетность</div>
    <div>
        Ежемесячно мы будем отправлять вам отчеты о проделанной работе. В них
        мы указываем ежедневные позиции сайта по каждому поисковому запросу.
    </div>
    <div class="g-margin-30-top  b-margin-text-bottom3 b-h2">Скидка</div>
    <div class="b-margin-text-bottom3">
        Для того чтобы получить скидку 5%, найдите на сайте macroindex.ru
        промокод.
    </div>
    <div class="g-margin-30-top  b-margin-text-bottom3 b-h2">Контакты</div>
    <div>
        Специалист по развитию веб-проектов
    </div>
    <div class="g-text-bold">
        Максим Богачев
    </div>
    <div>
        Рабочий: +7 (495) 740-22-81 доб. 502
    </div>
    <div>Мобильный: +7 (929) 958-20-53</div>
    <div>
        <a href="http://www.m.bogachev@macroindex.ru">m.bogachev@macroindex.ru</a>
    </div>
    <div>
        ООО «МакроИндекс»
    </div>
    <div>
        <a href="http://www.macroindex.ru/">http://www.macroindex.ru/</a>
        <img class="b-manager" src="/images/css/manager.png">
    </div>
</div>
<div class="label-danger b-label">Более 100 клиентов довольны результатами. Присоединяйтесь!</div>
<div class="b-list b-list_4">
    <div class="b-margin-text-top b-text-small">© Внимание: содержание данного документа и прилагающиеся материалы,
        использованные в работе над проектом, являются
        собственностью агентства «МакроИндекс» и представляют собой конфиденциальную информацию. Пожалуйста, не
        изменяйте, не
        редактируйте и не передавайте их третьим лицам без нашего письменного согласия.
    </div>
</div>