<html lang="ru">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/css/style.css">
</head>

<div class="b-list">
    <div class="g-float-right b-logo-block">
        <img src="/images/css/logo.png" class="g-float-right b-logo">
    </div>

    <div class="b-margin-text-bottom-power"></div>
</div>
<div class="b-label-danger b-label b-huge-label">Коммерческое предложение</div>
<div class="b-list">
    <div class="b-margin-text-bottom-power"></div>
    <div class="b-large-font b-margin-text-bottom-power">
        <div class="b-margin-text-bottom3"><span class="g-text-bold">Адрес сайта: </span><?php echo $model->url; ?>
        </div>
        <div class="b-margin-text-bottom3"><span
                class="g-text-bold">Дата: </span><?php echo date("d.m.Y", strtotime($model->date)); ?></div>
        <div class="b-margin-text-bottom3"><span
                class="g-text-bold">Регион продвижения: </span><?php echo $model->region; ?></div>
        <div class="b-margin-text-bottom"><span
                class="g-text-bold">Поисковая система: </span><?php echo $model->getSearchSystem($model->search_system); ?>
        </div>
        <div class="b-text-center">Это предложение действительно до <?php echo $model->period; ?></div>
    </div>
    <pagebreak suppress="off"/>

    <div class="g-margin-10-top">Благодарим Вас за интерес, проявленный к нашей компании.</div>
    <div class="g-margin-10-top">Мы искренне надеемся, что наше предложение ответит на большинство Ваших вопросов.</div>
    <div class="g-margin-30-top  b-margin-text-bottom3 b-h2">Стоимость продвижения</div>
    <?php $summ = 0; ?>
    <?php foreach ($keywords as $keyword => $price): ?>
        <?php $summ = $summ + $price; ?>
    <?php endforeach; ?>
    <?php $summ = $summ * 0.9; ?>
    <div class="g-margin-10-top">Стоимость продвижения <?php echo $summ; ?> рублей. Она включает в себя работы по
        оптимизации и
        наращиванию ссылочной массы сайта. В стоимость включена ежемесячная веб-
        аналитика*, которая позволит <span class="g-text-bold">увеличить продажи</span> на вашем сайте.
    </div>
    <div class="g-margin-30-top  b-margin-text-bottom3 b-h2">Стоимость запросов</div>
    <div class="g-margin-10-top g-float-right g-margin-10-bottom b-margin-text-bottom3">Таблица 1</div>
    <table class="b-table g-margin-20-top b-text-center b-margin-text-bottom3">
        <thead>
        <tr class="b-label-danger b-text-white">
            <th class="b-text-center">№</th>
            <th class="b-text-center g-vertical-align-middle">Поисковый запрос</th>
            <th class="b-text-center">Стоимость,руб</th>
        </tr>

        </thead>
        <tbody>
        <?php $strok = 1; ?>
        <?php foreach ($keywords as $keyword => $price): ?>
            <tr>
                <td><?php echo $strok; ?></td>
                <td class="b-text-left"><?php echo $keyword; ?></td>
                <td><?php echo $price; ?></td>
                <?php $strok++; ?>
            </tr>
        <?php endforeach; ?>
        <tr>
            <td></td>
            <td class="b-text-right g-text-bold">Персональная скидка</td>
            <td class="g-text-bold">10%</td>
        </tr>
        <tr>
            <td></td>
            <td class="g-text-bold b-text-right">Итого</td>
            <td><?php echo $summ; ?></td>
        </tr>
        </tbody>
    </table>
    <div><span class="g-text-bold">* Веб-аналитика</span>- это измерение и анализ информации о посетителях веб-сайта с
        целью
        его улучшения и повышения продающих качеств.
    </div>
    <pagebreak />
    <div class="g-margin-30-top  b-margin-text-bottom3 b-h2">Сроки</div>
    <div class="b-margin-text-bottom3">
        По мнению наших экспертов, вывод всех запросов займет 6 месяцев. Первые
        результаты вы сможете увидеть уже через 3 недели.
    </div>
    <div class="g-margin-30-top  b-margin-text-bottom3 b-h2">Гарантии</div>
    <div class="">Мы разделяем финансовые риски со своими клиентами, поэтому предоставляем
        гарантию 50 % на все запросы.
    </div>
    <div class="g-margin-30-top  b-margin-text-bottom3 b-h2">Этапы продвижения сайта</div>
    <table class="b-table g-margin-20-top b-margin-text-bottom3 b-text-center">
        <thead>
        <tr class="b-label-danger b-text-white">
            <th class="b-text-center g-vertical-align-middle">Этап</th>
            <th class="b-text-center">Перечень работ</th>
        </tr>
        <tr class="b-label-danger b-text-white">

        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Перед подписанием договора</td>
            <td>Экспресс-анализ сайта и конкурентов.
                Составление и согласование списка
                ключевых запросов.
                Заключение договора.
            </td>
        </tr>
        <tr>
            <td>Начальный</td>
            <td>Детальный аудит сайта. Подготовка
                стратегии продвижения.
            </td>
        </tr>
        <tr>
            <td>Составление карты релевантности</td>
            <td>Разбиение поисковых запросов по
                релевантным страницам.
            </td>
        </tr>
        <tr>
            <td>Работа с текстами</td>
            <td>Анализ текущих текстов и написание
                новых при необходимости.
            </td>
        </tr>
        <tr>
            <td>Внутренняя оптимизация</td>
            <td>Исправление ошибок обнаруженных
                на «начальном» этапе. Проведения
                работ по внутренней оптимизации
                сайта.
            </td>
        </tr>
        <tr>
            <td>Настройка статистических счетчиков</td>
            <td>Установка и настройка
                Яндекс.Метрики.
            </td>
        </tr>
        <tr>
            <td>Внешняя оптимизация сайта</td>
            <td>Анализ ссылочной массы
                конкурентов. Построение стратегии
                закупки ссылочной массы. Начало
                закупки ссылочной массы.
            </td>
        </tr>
        <tr>
            <td>Мониторинг результатов</td>
            <td>Мониторинг результатов
                продвижения и внесение
                корректировок в стратегию.
            </td>
        </tr>
        </tbody>
    </table>
    <pagebreak />
    <div class="g-margin-30-top  b-margin-text-bottom3 b-h2">Отчетность</div>
    <div>
        Ежемесячно мы будем отправлять вам отчеты о проделанной работе. В них
        мы указываем ежедневные позиции сайта по каждому поисковому запросу.
    </div>
    <div class="g-margin-30-top  b-margin-text-bottom3 b-h2">Скидка</div>
    <div class="b-margin-text-bottom3">
        Для того чтобы получить скидку 5%, найдите на сайте macroindex.ru
        промокод.
    </div>
    <div class="g-margin-30-top  b-margin-text-bottom3 b-h2">Контакты</div>
    <div>
        Специалист по развитию веб-проектов
    </div>
    <div class="g-text-bold">
        Максим Богачев
    </div>
    <div>
        Рабочий: +7 (495) 740-22-81 доб. 502
    </div>
    <div>Мобильный: +7 (929) 958-20-53</div>
    <div>
        <a href="http://www.m.bogachev@macroindex.ru">m.bogachev@macroindex.ru</a>
    </div>
    <div>
        ООО «МакроИндекс»
    </div>
    <div>
        <a href="http://www.macroindex.ru/">http://www.macroindex.ru/</a>
    </div>
    <img class="b-manager" src="/images/css/manager.png">
</div>
<div class=" b-label-danger b-label">Более 100 клиентов довольны результатами. Присоединяйтесь!</div>
<div class="b-list b-list_4">
    <div class="b-margin-text-top b-text-small">© Внимание: содержание данного документа и прилагающиеся материалы,
        использованные в работе над проектом, являются
        собственностью агентства «МакроИндекс» и представляют собой конфиденциальную информацию. Пожалуйста, не
        изменяйте, не
        редактируйте и не передавайте их третьим лицам без нашего письменного согласия.
    </div>

</div>
</html>