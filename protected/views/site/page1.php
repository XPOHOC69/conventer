<div class="b-list">
    <div class="pull-right b-logo-block">
        <img src="/images/css/logo.png" class="pull-right b-logo">
    </div>
    <div class="b-margin-text-bottom-power"></div>
</div>
<div class=" label-danger b-label b-huge-label">Коммерческое предложение</div>
<div class="b-list">
    <div class="b-margin-text-bottom-power"></div>
    <div class="b-large-font b-margin-text-bottom-power">
        <div class="b-margin-text-bottom3"><span class="g-text-bold">Адрес сайта: </span><?php echo $model->url; ?></div>
        <div class="b-margin-text-bottom3"><span class="g-text-bold">Дата: </span><?php echo date("d.m.Y",strtotime($model->date)); ?></div>
        <div class="b-margin-text-bottom3"><span
                class="g-text-bold">Регион продвижения: </span><?php echo $model->region; ?>
        </div>
        <div class="b-margin-text-bottom"><span
                class="g-text-bold">Поисковая система: </span><?php echo $model->getSearchSystem($model->search_system); ?></div>
        <div class="text-center">Это предложение действительно до <?php echo $model->period; ?></div>
    </div>
</div>
