
<div class="b-list-text">
    <div class="g-margin-10-top">Благодарим Вас за интерес, проявленный к нашей компании.</div>
    <div class="g-margin-10-top">Мы искренне надеемся, что наше предложение ответит на большинство Ваших вопросов.</div>
    <div class="g-margin-30-top  b-margin-text-bottom3 b-h2">Стоимость продвижения</div>
    <?php $summ = 0; ?>
    <?php foreach ($keywords as $keyword => $price): ?>
            <?php $summ = $summ + $price; ?>
    <?php endforeach; ?>
    <?php $summ=$summ*0.9; ?>
    <div class="g-margin-10-top">Стоимость продвижения <?php echo $summ; ?> рублей. Она включает в себя работы по оптимизации и
        наращиванию ссылочной массы сайта. В стоимость включена ежемесячная веб-
        аналитика*, которая позволит <span class="g-text-bold">увеличить продажи</span> на вашем сайте.
    </div>
    <div class="g-margin-30-top  b-margin-text-bottom3 b-h2">Стоимость запросов</div>
    <div class="g-margin-10-top pull-right g-margin-10-bottom b-margin-text-bottom3">Таблица 1</div>
    <table class="table table-bordered table-striped g-margin-20-top text-center b-margin-text-bottom3">
        <thead>
        <tr class="label-danger b-text-white">
            <th class="text-center td_center">№</th>
            <th class="text-center g-vertical-align-middle">Поисковый запрос</th>
            <th class="text-center">Стоимость,руб</th>
        </tr>

        </thead>
        <tbody>
        <?php $strok = 1;?>
        <?php foreach ($keywords as $keyword => $price): ?>
            <tr>
                <td><?php echo $strok; ?></td>
                <td class="text-left"><?php echo $keyword; ?></td>
                <td><?php echo $price; ?></td>
                <?php $strok++; ?>
            </tr>
        <?php endforeach; ?>
        <tr>
            <td></td>
            <td class="text-right g-text-bold">Персональная скидка</td>
            <td class="g-text-bold">10%</td>
        </tr>
        <tr>
            <td></td>
            <td class="g-text-bold text-right">Итого</td>
            <td><?php echo $summ; ?></td>
        </tr>
        </tbody>
    </table>
    <div><span class="g-text-bold">* Веб-аналитика</span>- это измерение и анализ информации о посетителях веб-сайта
        с
        целью
        его улучшения и повышения продающих качеств.
    </div>
</div>