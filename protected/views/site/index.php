<!DOCTYPE html>
<html lang="ru">
<head>
    <!-- Мы исползуем кодировку utf-8 во всех проектах. Кодировка указывается всегда первой в секции head -->
    <meta charset="utf-8">
    <title>Стандарты МакроИндекс. HTML5/CSS3. v1.1 от 30.01.2015 г.</title>
    <!-- Мета тег X-UA-Compatible управляет режимом отображением страниц в браузерах IE8+ -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Масштабирование на мобильных устройствах. Initial-scale=1 - без масштабирования -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- У элемента отсутствует атрибут type="text/css", т. к. он в HTML5 задается по умолчанию -->
    <!-- Файл содержит CSS-фреймворк Bootstrap. Его нельзя редактировать. Layer0 в нашем приложении -->
    <link rel="stylesheet" href="/css/vendor/bootstrap.min.css">
    <!-- Файл с темой нашего приложения. Стандартная тема CSS-фреймворка Bootstrap.
    В теме мы переопределяем глобальные свойства элементов, установленные Bootstrap. Layer1 (тема) в нашем приложении -->
    <link rel="stylesheet" href="/css/vendor/bootstrap-theme.min.css">
    <!-- Файл с глобальными селекторами. Layer2 (слой проекта) в нашем приложении -->
    <link rel="stylesheet" href="/css/vendor/global.css">
    <!-- Файл с основными селекторами. Layer3 (слой проекта) в нашем приложении -->
    <link rel="stylesheet" href="/css/project.css">
    <!-- rel="shortcut icon" для Яндекса. Подробную информацию можно найти в помощи Яндекс Вебмастер. -->
    <link type="image/png" rel="shortcut icon" href="/favicon.png">
</head>
<body>
<!-- Основное содержание страницы -->
<main>
    <div class="container">
        <div class="b-outer-window">
            <div class="row">

                <div class="col-sm-3">
                    <div class="b-vertical-line">
                        <h2 class="text-center">Создать группу</h2>
                        <?php $form = $this->beginWidget('CActiveForm', array(
                            'id'                   => 'group-form',
                            'enableAjaxValidation' => false,
                        )); ?>
                        <div class="row">
                            <?php echo $form->labelEx($modelGroup, 'name'); ?>
                            <?php echo $form->textField($modelGroup, 'name', array('class' => 'form-control')); ?>
                            <?php echo $form->error($modelGroup, 'name'); ?>
                        </div>
                        <div class="row">
                            <div class="col-lg-8 col-lg-offset-2">
                                <button type="submit" class="btn btn-primary">Создать</button>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div>
                    <div class="b-vertical-line">
                        <h2 class="text-center">История КП</h2>
                        <?php foreach ($groups as $group): ?>
                            <ol class="g-margin-10-top"><?php echo $group->name; ?>
                                <?php if (!empty($group->reports)): ?>
                                    <table>
                                        <?php foreach ($group->reports as $report): ?>
                                            <tr>
                                                <td><a href="/site/index/delete/<?php echo $report->id; ?>">X</a></td>
                                                <td><a href="/site/index/id/<?php echo $report->id; ?>">
                                                    <li class="b-spisok-otstup"><?php echo $report->url; ?>
                                                        - <?php echo $report->date; ?></li>
                                                </a></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </table>
                                <?php endif; ?>
                            </ol>
                        <?php endforeach; ?>
                    </div>
                </div>

                <div class="col-sm-8 col-sm-offset-1">
                    <div class="text-center">
                        <h1>Конвертер отчетов</h1>
                    </div>
                    <hr>
                    <?php $form = $this->beginWidget('CActiveForm', array(
                        'id'                   => 'report-form',
                        'enableAjaxValidation' => false,
                    )); ?>
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="form-group">
                                <label for="srok-prodvigheniya">Выберите группу:</label>
                                <?php echo $form->dropDownList($modelReport, 'fk_group_id',
                                    CHtml::listData(Group::model()->findAll(), 'id', 'name'),
                                    array('class' => 'form-control')) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="form-group">
                                <?php echo $form->labelEx($modelReport, 'period'); ?>
                                <?php echo $form->textField($modelReport, 'period',
                                    array('class' => 'form-control')); ?>
                                <?php echo $form->error($modelReport, 'period'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="form-group">
                                <?php echo $form->labelEx($modelReport, 'client'); ?>
                                <?php echo $form->textField($modelReport, 'client',
                                    array('class' => 'form-control')); ?>
                                <?php echo $form->error($modelReport, 'client'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="form-group">
                                <?php echo $form->labelEx($modelReport, 'url'); ?>
                                <?php echo $form->textField($modelReport, 'url', array('class' => 'form-control')); ?>
                                <?php echo $form->error($modelReport, 'url'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="form-group">
                                <?php echo $form->labelEx($modelReport, 'region'); ?>
                                <?php echo $form->textField($modelReport, 'region',
                                    array('class' => 'form-control')); ?>
                                <?php echo $form->error($modelReport, 'region'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="form-group">
                                <?php echo $form->labelEx($modelReport, 'date'); ?>
                                <?php //echo $form->textField($modelReport, 'date', array('class' => 'form-control')); ?>
                                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                    'name'        => 'Report[date]',
                                    'attribute'   => 'date',
                                    'language'    => 'ru',
                                    'model'       => $modelReport,
                                    'options'     => array(
                                        'showAnim'   => 'fold',
                                        'dateFormat' => 'yy.mm.dd',
                                    ),
                                    'htmlOptions' => array(
                                        'class' => 'form-control'
                                    ),
                                ));?>
                                <?php echo $form->error($modelReport, 'date'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <label>
                                Поисковая система:
                            </label>

                            <div class="form-group">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="Report[search_system]" id="optionsRadios1"
                                               value="0" checked>
                                        Yandex
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="Report[search_system]" id="optionsRadios2"
                                               value="1">
                                        Google
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="form-group">
                                <?php echo $form->labelEx($modelReport, 'keyword_price'); ?>
                                <?php echo $form->textArea($modelReport, 'keyword_price',
                                    array('class' => 'form-control', 'rows' => '6')); ?>
                                <?php echo $form->error($modelReport, 'keyword_price'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <button type="submit" class="btn btn-primary">Конвертировать</button>
                        </div>
                    </div>
                    <?php $this->endWidget(); ?>

                </div>
            </div>
        </div>
    </div>
</main>
<!-- /Основное содержание страницы -->


<!-- /Подвал страницы -->
<!-- JavaScript библиотеки подключаются внизу страницы, чтобы ускорить загрузку страницы -->
<script type="text/javascript" src="/js/vendor/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/vendor/jquery-2.1.3.min.js"></script>
<!-- Основной JS-файл -->
<script type="text/javascript" src="/js/project.js"></script>
</body>
</html>