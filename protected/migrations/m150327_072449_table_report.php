<?php

class m150327_072449_table_report extends CDbMigration
{
    protected $options = 'ENGINE=InnoDB CHARSET=utf8 COMMENT="Отчеты"';

    public function up()
    {
        $this->createTable('report', array(
                'id'            => 'pk',
                'client'        => 'VARCHAR(50) NOT NULL COMMENT "Имя клиента"',
                'url'           => 'VARCHAR(50) NOT NULL COMMENT "Адрес сайта"',
                'region'        => 'VARCHAR(50) NOT NULL COMMENT "Регион продвижения"',
                'date'          => 'DATE NOT NULL COMMENT "Дата"',
                'period'        => 'VARCHAR(50) NOT NULL COMMENT "Срок продвижения"',
                'search_system' => 'BOOL NOT NULL COMMENT "Поисковая система"',
                'keyword_price' => 'TEXT NOT NULL COMMENT "Ключевая фраза-цена"',
                'fk_group_id'   => 'INT COMMENT "Связь на группу"',

            ),
            $this->options);
        $this->addForeignKey('fk_report_group_id', 'report', 'fk_group_id',
            'group', 'id', 'RESTRICT', 'RESTRICT');
    }

    public function down()
    {
        $this->dropForeignKey('fk_report_group_id', 'report');
        $this->dropTable('report');
    }
}