<?php

class m150327_072400_table_group extends CDbMigration
{
    protected $options = 'ENGINE=InnoDB CHARSET=utf8 COMMENT="Группы"';

    public function up()
    {
        $this->createTable('group', array(
                'id'           => 'pk',
                'name'         => 'VARCHAR(50) UNIQUE NOT NULL COMMENT "Название группы"',
            ),
            $this->options);

    }

    public function down()
    {
        $this->dropTable('group');
    }
}